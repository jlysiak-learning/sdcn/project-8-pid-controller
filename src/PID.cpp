#include "PID.h"

#include <limits>


PID::PID(double Kp_, double Ki_, double Kd_) :
        Kp(Kp_), Ki(Ki_), Kd(Kd_) {
    reset();

    m = -std::numeric_limits<double>::infinity();
    M = std::numeric_limits<double>::infinity();
}

void PID::clamp(double m, double M) {
    this->m = m;
    this->M = M;
}

double PID::update(double current) {
    double err = current - setpoint_;
    double last_in_queue_err = err_queue[err_queue_idx];
    double recent_err = err_queue[(err_queue_idx - 1 + err_queue_size) % err_queue_size];
    current_ = current;
    err_queue[err_queue_idx] = err;
    err_queue_idx = (err_queue_idx + 1) % err_queue_size;
    err_queue_sum += err - last_in_queue_err;

    p_error = - err * Kp;
    d_error = - (err - recent_err) * Kd;
    i_error = - err_queue_sum * Ki;

    output_ = p_error + d_error + i_error;

    if (output_ < m) {
        output_ = m;
    } else if (output_ > M) {
        output_ = M;
    }
    log();
    return output_;
}

void PID::log() {
    if (!logfile.is_open()) {
        return;
    }
    logfile << setpoint_ << "," << current_ << "," << p_error << ",";
    logfile << d_error << "," << i_error << "," << output_ << std::endl;
}

void PID::reset() {
    p_error = 0;
    i_error = 0;
    d_error = 0;

    err_queue_sum = 0;
    err_queue_idx = 0;
    for (int i = 0; i < err_queue_size; ++i) {
        err_queue[i] = 0;
    }
    err_queue_sum = 0;
}

void PID::setpoint(double setpoint) {
    setpoint_ = setpoint;
}

void PID::setup_logger(std::string  path) {
    logfile_name = path;
    logfile.open(path, std::fstream::out);
    logfile << "setpoint,current,p_error,d_error,i_error,output" << std::endl;
}
