# Self-Driving Car Nanodegree project: PID controller

Starter code for this project is [here](https://github.com/udacity/CarND-PID-Control-Project).

Term2 simulator can be found [here](https://github.com/udacity/self-driving-car-sim/releases).

## Directory contents

- `src`: PID controller code
- `install-linux.sh`: installs required tools, like cmake, gcc, libssl, etc.
  Also, downloads `uWebSockets` from github, builds it and copies important
  files: `libuWS.so` and include files.
- `build.sh`: just runs cmake and make
- `clean.sh`: ...
- `run.sh`: runs the app
- `vis.py`: visualize PID data

## Building and running the code

1. Run `./install.sh` to install deps
2. Run `./build.sh`
3. Run Term2 sim and select PID controller project
4. Run `./run.sh Kp Kd Ki Kp' Kd' Ki' V b`, where:
    - `Kp, Kd, Ki` are steering PID controller coefficients
    - `Kp', Kd', Ki'` are speed PID controller coefficients
    - `V` car max speed
    - `b` speed off-the-track-center penalty, so  speed setpoint is computed as
      follows: `v_setpoint = V - abs(Cross Track Error) * b`
5. To visualize how PID controlles work run:
    - `./vis.py build/pid_steer.log` for steering PID
    - `./vis.py build/pid_speed.log` for speed PID

    However the script is not very spohisticated and plots have some readability
    issues, but it's enough for a simple debugging.

## Results

A successful lap is shown in the [video](./examples/result.mp4).

PID controller implementation is in `src/PID.*` files.
The controller stores last 50 errors in a circular buffer and their sum as a
separate variable. Update off the sum takes then only one addition and one
subtraction.
The sum of last 50 errors is used instead of full discrete integral over time
from the beginning to get rid of growing I part of controller to very large
values.
I part of the controller helps when a car drives on long turns, where it has to
maintain non-zero steering angle for a long time.

Speed of the car is also controlled by a PID controller.
Current speed setpoint is set to: `v_setpoint = V - abs(Cross Track Error) * b`
If the car approaches a turn it goes a bit  off the center and automatically
slows down.

## Tuning

Tuning procedure was as follows:
```
./run.sh 0.1 0 0.0 0.2 0.05 0 30 6 - oscillations
./run.sh 0.1 1 0.0 0.2 0.05 0 30 6 - very small oscillations, but sometimes cas goes off the track
./run.sh 0.1 1.8 0.0 0.2 0.05 0 30 6 - pretty stable (sometimes stiff), but has issues with turning, needs a bit of bias 
./run.sh 0.1 1.8 0.001 0.2 0.05 0 30 6 - still bit stiff, touches edges a bit on the tight turns
./run.sh 0.1 1.8 0.002 0.2 0.05 0 30 6 - ok, does not touch edges
```
Not so long and sophisticated but enough to achieve driving car.

## Discussion
### P
Works like a spring. Basically, the system with only P term is an oscillator, as
the force driving the system to the equilibrium is proportional to a distance
from the equilibrium, simply `m*x'' = -k*x` :) (mass on the spring)

### D
Introduces dumping. This part of the controller tries to suppress a rate of
change, the bigger "velocity" of error in time, greater value of D part.

### I
If the system is biased, accumulated error over time in the integral can cause
that the system overcome bias and will back to equilibrium.

