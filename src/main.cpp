#include <math.h>
#include <uWS/uWS.h>
#include <iostream>
#include <string>
#include "json.hpp"
#include "PID.h"


// for convenience
using nlohmann::json;
using std::string;

// For converting back and forth between radians and degrees.
constexpr double pi() { return M_PI; }
double deg2rad(double x) { return x * pi() / 180; }
double rad2deg(double x) { return x * 180 / pi(); }

// Checks if the SocketIO event has JSON data.
// If there is data the JSON object in string format will be returned,
// else the empty string "" will be returned.
string hasData(string s) {
  auto found_null = s.find("null");
  auto b1 = s.find_first_of("[");
  auto b2 = s.find_last_of("]");
  if (found_null != string::npos) {
    return "";
  }
  else if (b1 != string::npos && b2 != string::npos) {
    return s.substr(b1, b2 - b1 + 1);
  }
  return "";
}

int main(int argc, char** argv) {
  uWS::Hub h;

  if (argc < 9) {
      std::cout << "Usage: " << argv[0] << " Kp Kd Ki Kp' Kd' Ki' max_speed turn_speed_penalty" << std::endl;
      std::cout << "- Kp Kd Ki: steering PID params" << std::endl;
      std::cout << "- Kp' Kd' Ki': speed PID params" << std::endl;
      exit(0);
  }

  double Kp = std::atof(argv[1]);
  double Kd = std::atof(argv[2]);
  double Ki = std::atof(argv[3]);
  double Kp_ = std::atof(argv[4]);
  double Kd_ = std::atof(argv[5]);
  double Ki_ = std::atof(argv[6]);
  double speed_max = std::atof(argv[7]);
  double speed_turning_penalty = std::atof(argv[8]);
  std::string steer_pid_log = "pid_steer.log";
  std::string speed_pid_log = "pid_speed.log";

  PID steer_pid(Kp, Ki, Kd);
  PID speed_pid(Kp_, Ki_, Kd_);

  steer_pid.clamp(-1, 1);
  steer_pid.setpoint(0);
  steer_pid.setup_logger(steer_pid_log);

  speed_pid.clamp(-1, 1);
  speed_pid.setup_logger(speed_pid_log);

  h.onMessage([&speed_turning_penalty, &speed_max, &steer_pid, &speed_pid](uWS::WebSocket<uWS::SERVER> ws, char *data, size_t length, 
                     uWS::OpCode opCode) {
    // "42" at the start of the message means there's a websocket message event.
    // The 4 signifies a websocket message
    // The 2 signifies a websocket event
    if (length && length > 2 && data[0] == '4' && data[1] == '2') {
      auto s = hasData(string(data).substr(0, length));

      if (s != "") {
        auto j = json::parse(s);

        string event = j[0].get<string>();

        if (event == "telemetry") {
          // j[1] is the data JSON object
          double cte = std::stod(j[1]["cte"].get<string>());
          double speed = std::stod(j[1]["speed"].get<string>());
          double angle = std::stod(j[1]["steering_angle"].get<string>());
          double steer_value;
          double throttle;
          double speed_setpoint = std::max(5., speed_max - speed_turning_penalty * std::abs(cte));
          speed_pid.setpoint(speed_setpoint);

          steer_value = steer_pid.update(cte);
          throttle = speed_pid.update(speed);

          // DEBUG
          std::cout << "CTE: " << cte << " Steering Value: " << steer_value << " angle: " << angle << std::endl;

          json msgJson;
          msgJson["steering_angle"] = steer_value;
          msgJson["throttle"] = throttle;
          auto msg = "42[\"steer\"," + msgJson.dump() + "]";
          ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);
        }  // end "telemetry" if
      } else {
        // Manual driving
        string msg = "42[\"manual\",{}]";
        ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);
      }
    }  // end websocket message if
  }); // end h.onMessage

  h.onConnection([&h](uWS::WebSocket<uWS::SERVER> ws, uWS::HttpRequest req) {
    std::cout << "Connected!!!" << std::endl;
  });

  h.onDisconnection([&h](uWS::WebSocket<uWS::SERVER> ws, int code, 
                         char *message, size_t length) {
    ws.close();
    std::cout << "Disconnected" << std::endl;
  });

  int port = 4567;
  if (h.listen(port)) {
    std::cout << "Listening to port " << port << std::endl;
  } else {
    std::cerr << "Failed to listen to port" << std::endl;
    return -1;
  }
  
  h.run();
}
