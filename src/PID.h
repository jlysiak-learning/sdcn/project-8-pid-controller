#ifndef PID_H
#define PID_H

#include <string>
#include <fstream>

class PID {
 public:
  PID(double Kp_, double Ki_, double Kd_);

  /** Update the PID controller with current error.
   * @param err The current error */
  double update(double err);

  void clamp(double m, double M);
  void setpoint(double setpoint);
  void setup_logger(std::string path);

 private:
  void reset();
  void log();

 private:
  std::string logfile_name;
  std::fstream logfile;

  double current_;
  double setpoint_;
  double output_;

  double m;
  double M;

  /** PID Errors */
  double p_error;
  double i_error;
  double d_error;

  static const int err_queue_size = 50;
  int err_queue_idx = 0;
  double err_queue[err_queue_size];
  double err_queue_sum;

  /** PID Coefficients */
  double Kp;
  double Ki;
  double Kd;
};

#endif  // PID_H
