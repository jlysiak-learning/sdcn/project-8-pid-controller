#!/bin/bash

set -x

source /etc/os-release || echo 'Warning: /etc/os-release was not found'

if [[ "$ID" == 'arch' ]]; then
    sudo pacman -S git libuv openssl gcc cmake make
else
    echo 'Assuming Debian-like distro'

    sudo apt-get update
    sudo apt-get install git libuv1-dev libssl-dev gcc g++ cmake make
fi


TMP=/tmp/uwebsock

echo "Installing uWebSockets locally..."

git clone https://github.com/uWebSockets/uWebSockets $TMP
pushd .
cd $TMP
git checkout e94b6e1
mkdir build
cd build
cmake ..
make
popd

mkdir -p deps/lib deps/include/uWS

cp $TMP/build/libuWS.so deps/lib/libuWS.so
cp -r $TMP/src/*.h deps/include/uWS
rm -rf $TMP
