#!/bin/bash
# Script to build all components from scratch, using the maximum available CPU power
#
# Given parameters are passed over to CMake.
# Examples:
#    * ./build.sh -DCMAKE_BUILD_TYPE=Debug
#    * ./build.sh VERBOSE=1
#
# Written by Tiffany Huang, 12/14/2016
# Modified by Jacek Lysiak 
#

# Go into the directory where this bash script is contained.
cd `dirname $0`

set -x
N=$(( `cat /proc/stat | grep cpu | wc -l` - 1 ))
# Compile code.
mkdir -p build
cd build
cmake ..
make -j $N $*
