#!/usr/bin/env python3


import matplotlib.pyplot as plt
import pandas as pd



def show(f):
    data = pd.read_csv(f)

    data.plot(y=["setpoint", "current", "p_error", "d_error", "i_error", "output"], title=f)
    plt.show()

if __name__ == "__main__":
    import sys
    show(sys.argv[1])
